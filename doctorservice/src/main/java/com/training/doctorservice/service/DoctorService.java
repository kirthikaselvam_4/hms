package com.training.doctorservice.service;



import java.util.List;

import javax.validation.Valid;

import com.training.doctorservice.model.Doctor;

public interface DoctorService {
	
	/*public List<Doctor> getAllDoctorDetails();
	public Doctor getOneDoctor(Long doctorId);

	

	public Doctor updateDoctorDetails(Doctor doctor);

	public boolean deleteDoctor(Long doctorId);*/
	
	public List<Doctor> getDoctors();
	public Doctor createDoctor(Doctor doctor);
   // public List<Doctor> getDoc(long doctorId);
	public Doctor getOneDoctor(long doctorId);
	public boolean deleteDoctor(long doctorId);
	public Doctor updateDoctorDetails(@Valid Doctor doctor);
}