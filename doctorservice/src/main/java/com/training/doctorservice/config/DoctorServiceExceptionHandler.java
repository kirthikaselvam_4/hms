package com.training.doctorservice.config;

	
	import java.util.HashMap;
	import java.util.Map;

	import org.hibernate.exception.JDBCConnectionException;
	//import org.springframework.data.rest.webmvc.ResourceNotFoundException;
	import org.springframework.http.HttpHeaders;
	import org.springframework.http.HttpStatus;
	import org.springframework.http.ResponseEntity;
	import org.springframework.validation.FieldError;
	import org.springframework.web.bind.MethodArgumentNotValidException;
	import org.springframework.web.bind.annotation.ControllerAdvice;
	import org.springframework.web.bind.annotation.ExceptionHandler;
	import org.springframework.web.context.request.WebRequest;
	import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

	@ControllerAdvice
	public class DoctorServiceExceptionHandler extends ResponseEntityExceptionHandler {

		@Override
		protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
				HttpHeaders headers, HttpStatus status, WebRequest request) {
			Map<String, String> errors = new HashMap<>();
			ex.getBindingResult().getAllErrors().forEach((error) -> {
				String fieldName = ((FieldError) error).getField();
				String message = error.getDefaultMessage();
				errors.put(fieldName, message);

			});
			return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
		}
		@ExceptionHandler(value = DoctorNotFoundException.class)
		public ResponseEntity<String> exception(DoctorNotFoundException exception) {
			return new ResponseEntity<>("Doctor not found in the databsae, please try again with the different DoctorId",
					HttpStatus.NOT_FOUND);
		}
		@ExceptionHandler(value = { JDBCConnectionException.class })
		public <ResourceNotFoundException> ResponseEntity<String> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {

			return new ResponseEntity<String>("Database is down, please try after some time", HttpStatus.NOT_FOUND);
		}
}
