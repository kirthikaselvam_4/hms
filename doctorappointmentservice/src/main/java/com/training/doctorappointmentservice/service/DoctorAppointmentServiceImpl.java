package com.training.doctorappointmentservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.training.doctorappointmentservice.model.Appointment;
import com.training.doctorappointmentservice.model.Doctor;


@Service
public class DoctorAppointmentServiceImpl implements DoctorAppointmentService {

	@Autowired
	DoctorWebService docWebSevice;

	@Autowired
	AppointmentWebService appointmentWebService;

	@Autowired 
	DoctorWebService doctorWebService;
	
	
	
	/*@Override
	public List<Doctor> getDoctors() {
	List<Doctor> docList=doctorWebService.getDoctors();
	System.out.println();
	for(Doctor doctor:docList)
	{
		doctor.setAppointment(appointmentWebService.getAppointmentByDoctorId(doctor.getDoctorId()));
	}
	return docList;
	}*/

	@Override
	public Doctor addDoctorAppointment(Doctor doctor) {
		doctor.setAppointment(appointmentWebService.addDoctor(doctor.getAppointment()));
		return doctor;
	}
	
	@Override
	public List<Doctor> getDoctors() {
	List<Doctor> docList=doctorWebService.getDoctors();
	System.out.println();
	for(Doctor doctor:docList)
	{
		doctor.setAppointment(appointmentWebService.getAppointmentByDoctorId(doctor.getDoctorId()));
	}
	return docList;
	}

	@Override
	public Doctor getOneDoctor(long doctorId) {
		Doctor doc=doctorWebService.getOneDoctor(doctorId);
		 doc.setAppointment(appointmentWebService.getAppointmentByDoctorId(doc.getDoctorId()));
		 return doc;
	}

	@Override
	public String deleteDoctorDetailsById(long doctorId) {
		String doc=doctorWebService.deleteDoctorDetailsById(doctorId);
		String app=appointmentWebService.deleteAppointmentByDoctorId(doctorId);
		return doc+" "+app+" for doctorid "+doctorId;
	}

	@Override
	public Doctor updateAppointmentByDoctorId(Doctor doctor) {
		// TODO Auto-generated method stub
		//doctorWebService.updateDoctorDetails(doctor);
		doctor.setAppointment(appointmentWebService.updateAppointmentByDoctorId(doctor.getAppointment()));
		return doctor;
	}
	
	
}
