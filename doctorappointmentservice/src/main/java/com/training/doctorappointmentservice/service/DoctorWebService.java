package com.training.doctorappointmentservice.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.training.doctorappointmentservice.model.Doctor;

@FeignClient("DOCTORSERVICE")
public interface DoctorWebService {

	@GetMapping("/doctors")
	public List<Doctor> getDoctors();

	@PostMapping("/doctor")
	public Doctor addDoctor(Doctor doctor);

	@GetMapping(value = "doctor/{doctorId}")
    public Doctor getOneDoctor(@PathVariable long doctorId);
	
	@DeleteMapping(value = "/{doctorId}")
	public String deleteDoctorDetailsById(@PathVariable long doctorId);
	
	@PutMapping(value = "/doc")
	public Doctor updateDoctorDetails(Doctor doctor);
}
