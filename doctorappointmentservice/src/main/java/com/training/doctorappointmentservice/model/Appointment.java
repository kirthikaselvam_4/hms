package com.training.doctorappointmentservice.model;

import java.sql.Date;
import java.sql.Time;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Appointment {

	private long appointmentId;
	@DateTimeFormat(pattern ="yyyy-MM-dd")
	private Date appointmentDate;
	@DateTimeFormat(pattern="HH:mm:ss")
	private Time appointmentTime;
    private String Status;
	private long patientId;
	private long doctorId;
	
}