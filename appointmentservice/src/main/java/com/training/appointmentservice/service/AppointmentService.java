package com.training.appointmentservice.service;

import java.util.List;

import com.training.appointmentservice.model.Appointment;

public interface AppointmentService  {
	
    public Appointment getAppointmentByDoctorId(long doctorId);
    public Appointment addAppointment(Appointment doctor);
	public boolean deleteAppointmentByDoctorId(long doctorId);
	public Appointment updateAppointmentByDoctorId(Appointment appointment);
	public List<Appointment> getAppointments();
	
	
}


/*public List<Appointment> getAllAppointmentDetails();
public Appointment getOneAppointment(Long appointmentId);

public Appointment createAppointmentDetails(Appointment appointment);

public Appointment updateAppointmentDetails(Appointment appointment);

public boolean deleteAppointment(Long appointmentId);
//public Appointment addAppointment(Appointment doctor);
	//public List<Appointment> getAppointment();
	//public List<Appointment> getAllAppointmentByDoctorId(long doctorId);*/
