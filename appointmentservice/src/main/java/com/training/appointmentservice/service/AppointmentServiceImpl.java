package com.training.appointmentservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.appointmentservice.model.Appointment;
import com.training.appointmentservice.repository.AppointmentRepository;

//import com.training.doctorappointmentservice.model.Doctor;
@Service
public class AppointmentServiceImpl implements AppointmentService {
	@Autowired
	AppointmentRepository doctorRepository;

	@Override
	public Appointment getAppointmentByDoctorId(long doctorId) {
		return doctorRepository.getAppointmentByDoctorId(doctorId);

	}

	@Override
	public Appointment addAppointment(Appointment doctor) {
		return doctorRepository.save(doctor);
	}

	@Override
	public boolean deleteAppointmentByDoctorId(long doctorId) {
        doctorRepository.deleteAppointmentByDoctorId(doctorId);
		return true;
	}

	@Override
	public Appointment updateAppointmentByDoctorId(Appointment doctor) {

		return doctorRepository.save(doctor);
	}

	@Override
	public List<Appointment> getAppointments() {

		return doctorRepository.findAll();
	}

}
/*
 * public Address createAddress(Address employee) { return
 * empRepository.save(employee); doctorRepository.deleteById(doctorId); public
 * List<Doctor> getDoctors() { List<Doctor>
 * docList=doctorWebService.getDoctors(); } /*@Override public List<Appointment>
 * getAllAppointmentByDoctorId(long doctorId) { // TODO Auto-generated method
 * stub return doctorRepository.getAllAppointmentByDoctorId(doctorId); }
 */

/*
 * @Override public Address getAddressByEmployeeId(long employeeId) { return
 * empRepository.getAddressByEmployeeId(employeeId); }
 */

/*
 * @Override public List<Appointment> getAllAppointmentDetails() { // TODO
 * Auto-generated method stub return appointmentRepository.findAll(); }
 * 
 * @Override public Appointment getOneAppointment(Long appointmentId) { // TODO
 * Auto-generated method stub return
 * appointmentRepository.findById(appointmentId).orElse(new Appointment()); }
 * 
 * @Override public Appointment createAppointmentDetails(Appointment
 * appointment) { // TODO Auto-generated method stub
 * 
 * return appointmentRepository.save(appointment); } @Override public
 * Appointment updateAppointmentDetails(Appointment appointment) { // TODO
 * Auto-generated method stub
 * 
 * return appointmentRepository.save(appointment); } @Override public boolean
 * deleteAppointment(Long appointmentId) { // TODO Auto-generated method stub
 * appointmentRepository.deleteById(appointmentId); return true; }
 */
//@Override
//public Appointment addAppointment(Appointment doctor) {
// TODO Auto-generated method stub
// return doctorRepository.save(doctor);
//}
/*
 * @Override public List<Appointment> getAppointment() { return
 * doctorRepository.findAll(); }
 */